ThisBuild / scalaVersion := "2.13.6"

lazy val calculator = (project in file("."))
  .settings(
    name := "Calculator",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.7" % Test,
  )
