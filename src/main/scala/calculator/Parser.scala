package parser {

  // Token Types
  sealed class Token
  case class TokenNum(num: Int) extends Token
  case class TokenOp(op: String) extends Token
  case class TokenEOF() extends Token
  case class TokenError(str: String) extends Token
  case class TokenLparen() extends Token
  case class TokenRparen() extends Token

  // companion object for lexer
  object Lexer {
    val Whitespace = "(\\s+)(.*)".r
    val Number = "(\\d+)(.*)".r
    val Operator = "([+\\-/*^])(.*)".r
    val Other = "(.)(.*)".r
    val Lparen = "\\((.*)".r
    val Rparen = "\\)(.*)".r
  }

  class Lexer(s: String) {
    import Lexer._
    var str = s
    def next_token(): Token =
      str match {
        case Whitespace(_, rest) => str = rest; next_token()
        case Number(num, rest)   => str = rest; TokenNum(num.toInt)
        case Operator(op, rest)  => str = rest; TokenOp(op)
        case Lparen(rest)        => str = rest; TokenLparen()
        case Rparen(rest)        => str = rest; TokenRparen()
        case ""                  => TokenEOF()
        case Other(e, rest)      => str = rest; TokenError(e)
      }

    def set_string(s: String) = str = s
  }

  // AST Types
  class Expr
  case class Num(num: Int) extends Expr
  case class UnOp(op: String, rhs: Expr) extends Expr
  case class BinOp(lhs: Expr, op: String, rhs: Expr) extends Expr

  class ParserException(message: String) extends Exception(message)

  class Parser(l: Lexer) {

    var curr: Token = l.next_token()
    private def advance() = curr = l.next_token()

    def set_input(s: String) = {
      l.set_string(s)
      advance()
    }

    def parse(): Expr = parse_expr()

    private def parse_expr(): Expr = {
      val v = parse_plus_min()
      curr match {
        case TokenEOF() => v
        case _          => throw new ParserException(s"unexpected token $curr")
      }
      v
    }

    private def parse_plus_min(): Expr = {
      def match_recursive(lhs: Expr): Expr = {
        curr match {
          case TokenOp(op) if (op == "+" || op == "-") => {
            advance()
            match_recursive(BinOp(lhs, op, parse_times_div()))
          }
          case _ => lhs
        }
      }
      match_recursive(parse_times_div())
    }

    private def parse_times_div(): Expr = {
      def match_recursive(lhs: Expr): Expr = {
        curr match {
          case TokenOp(op) if (op == "*" || op == "/") => {
            advance()
            match_recursive(BinOp(lhs, op, parse_pow()))
          }
          case _ => lhs
        }
      }
      match_recursive(parse_pow())
    }

    private def parse_pow(): Expr = {
      def match_recursive(lhs: Expr): Expr = {
        curr match {
          case TokenOp("^") => {
            advance()
            match_recursive(BinOp(lhs, "^", parse_pow()))
          }
          case _ => lhs
        }
      }
      match_recursive(parse_unary())
    }

    private def parse_unary(): Expr = {
      curr match {
        case TokenOp("-") => advance(); UnOp("-", parse_atom())
        case _            => parse_atom()
      }
    }

    private def parse_atom(): Expr = {
      curr match {
        case TokenNum(num) => advance(); Num(num)
        case TokenLparen() => {
          advance()
          val expr = parse_plus_min()
          curr match {
            case TokenRparen() => advance(); expr
            case _             => throw new ParserException(s"unmatched parens")
          }
        }
        case TokenError(got) =>
          throw new ParserException(s"unrecognize input $got")
        case v => throw new ParserException(s"unexpected token $v")
      }
    }
  }

  object Interpreter {
    import scala.math.pow
    def eval(e: Expr): Double = {
      e match {
        case Num(num)       => num
        case UnOp("-", rhs) => -eval(rhs)
        case BinOp(lhs, op, rhs) => {
          op match {
            case "+" => eval(lhs) + eval(rhs)
            case "-" => eval(lhs) - eval(rhs)
            case "*" => eval(lhs) * eval(rhs)
            case "/" => {
              eval(rhs) match {
                case 0.0   => throw new ParserException("/ by zero")
                case denom => eval(lhs) / denom
              }
            }
            case "^" => pow(eval(lhs), eval(rhs))
          }
        }
      }
    }
  }
}
