package calculator {
  import scala.io.StdIn.readLine
  import util.control.Breaks._
import parser.ParserException

  object Calc extends App {
    val calc = new Calculator()
    println("This is a calculator. to exit enter \"q\"")
    breakable {
      while (true) {
        val input = readLine("$> ")
        if (input == "q") break
        try {
          println(calc.calculate(input))
        } catch {
          case e: ParserException => println(e.getMessage())
        }
      }
    }
  }
}
