package calculator {
  import parser._

  class Calculator {
    val parser = new Parser(new Lexer(""))

    def calculate(s: String) = {
      parser.set_input(s)
      Interpreter.eval(parser.parse())
    }
  }
}
