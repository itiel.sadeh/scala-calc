import org.scalatest.funsuite._
import parser._
import calculator._
import java.io.EOFException
import java.text.ParseException
import scala.concurrent.duration.Duration

class LexerSuite extends AnyFunSuite {
  test("lexer with empty string should return EOF") {
    assert(new Lexer("").next_token() == TokenEOF())
  }

  test("lexer skip whitespaces") {
    assert(new Lexer(" \t\n").next_token() == TokenEOF())
  }

  test("lexer recognize numbers") {
    val l = new Lexer("42")
    assert(l.next_token() == TokenNum(42))
    assert(l.next_token() == TokenEOF())
  }

  test("lexer recognize operators") {
    val l = new Lexer("-+*/^")
    assert(l.next_token() == TokenOp("-"))
    assert(l.next_token() == TokenOp("+"))
    assert(l.next_token() == TokenOp("*"))
    assert(l.next_token() == TokenOp("/"))
    assert(l.next_token() == TokenOp("^"))
    assert(l.next_token() == TokenEOF())
  }

  test("lexer recognize parentheses") {
    val l = new Lexer("()")
    assert(l.next_token() == TokenLparen())
    assert(l.next_token() == TokenRparen())
    assert(l.next_token() == TokenEOF())
  }

  test("unrecognize symbols results in TokenError") {
    val l = new Lexer("d!.")
    assert(l.next_token() == TokenError("d"))
    assert(l.next_token() == TokenError("!"))
    assert(l.next_token() == TokenError("."))
    assert(l.next_token() == TokenEOF())
  }

  test("set_string should set a new string for lexing") {
    val l = new Lexer("")
    assert(l.next_token() == TokenEOF())
    l.set_string("3")
    assert(l.next_token() == TokenNum(3))
    assert(l.next_token() == TokenEOF())
  }
}

class ParserSuite extends AnyFunSuite {

  def create_parser(s: String) = new Parser(new Lexer(s))

  test("empty parser should result in an error") {
    val p = create_parser("")
    assertThrows[ParserException] {
      p.parse()
    }
  }

  test("unexpected end of input should throw") {
    val p = create_parser("3/")
    assertThrows[ParserException] {
      p.parse()
    }
  }

  test("unrecognize input should throw") {
    val p = create_parser("d")
    assertThrows[ParserException] {
      p.parse()
    }
  }

  test("one digit number AST") {
    val p = create_parser("2")
    assert(p.parse() == Num(2))
  }

  test("multi digit number AST") {
    val p = create_parser("42")
    assert(p.parse() == Num(42))
  }

  test("unary minus expression AST") {
    val p = create_parser("-1")
    val expected_ast = UnOp(op = "-", rhs = Num(1))
    assert(p.parse() == expected_ast)
  }

  test("unary and binary expression") {
    val p = create_parser("4 + -3")
    val expected_ast = BinOp(
      lhs = Num(4),
      op = "+",
      rhs = UnOp(op = "-", rhs = Num(3))
    )
    assert(p.parse() == expected_ast)
  }

  test("unary minus before parentheses") {
    val p = create_parser("4 * -(3 + 2)")
    val expected_ast = BinOp(
      lhs = Num(4),
      op = "*",
      rhs = UnOp(op = "-", rhs = BinOp(lhs = Num(3), op = "+", rhs = Num(2)))
    )
    assert(p.parse() == expected_ast)
  }

  test("basic binary operation AST") {
    val p = create_parser("4 + 3")
    val expected_ast = BinOp(lhs = Num(4), op = "+", rhs = Num(3))
    assert(p.parse() == expected_ast)
  }

  test("test left associativity") {
    val p = create_parser("4 + 3 + 2")
    val expected_ast = BinOp(
      lhs = BinOp(lhs = Num(4), op = "+", rhs = Num(3)),
      op = "+",
      rhs = Num(2)
    )
    assert(p.parse() == expected_ast)
  }

  test("test right associativity") {
    val p = create_parser("2 ^ 3 ^ 2")
    val expected_ast = BinOp(
      lhs = Num(2),
      op = "^",
      rhs = BinOp(lhs = Num(3), op = "^", rhs = Num(2))
    )
    assert(p.parse() == expected_ast)
  }

  test("test precedence order") {
    val p = create_parser("4 + 3 * 2")
    val expected_ast = BinOp(
      lhs = Num(4),
      op = "+",
      rhs = BinOp(lhs = Num(3), op = "*", rhs = Num(2))
    )
    assert(p.parse() == expected_ast)
  }

  test("parentheses imply order") {
    val p = create_parser("(4 + 3) * 2")
    val expected_ast = BinOp(
      lhs = BinOp(lhs = Num(4), op = "+", rhs = Num(3)),
      op = "*",
      rhs = Num(2)
    )
    assert(p.parse() == expected_ast)
  }

}

class CalculatorSuite extends AnyFunSuite {

  val calc = new Calculator()

  test("test basic input") {
    assert(calc.calculate("1") == 1.0)
  }

  test("divide by zero should throw") {
    assertThrows[ParserException] {
      calc.calculate("1 / 0")
    }
  }

  test("test one operator expressions") {
    assert(calc.calculate("1 + 1") == 2.0)
    assert(calc.calculate("1 - 1") == 0.0)
    assert(calc.calculate("1 * 1") == 1.0)
    assert(calc.calculate("1 / 1") == 1.0)
    assert(calc.calculate("1 ^ 1") == 1.0)
    assert(calc.calculate("-1") == -1.0)
  }

  test("test complex expressions") {
    assert(calc.calculate("1 + 2 * 3") == 7)
    assert(calc.calculate("-1 * 4 ^ -2") == -.0625)
    assert(calc.calculate("-(2 - 3) - -(4 * 5)") == 21)
  }

}
